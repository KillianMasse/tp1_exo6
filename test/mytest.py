# -*- coding: utf-8 -*-

import unittest
import logging

from Calculator.simplecalculator import SimpleCalculator as SimpleCalculator


class AdditionTest(unittest.TestCase):
    """
    Classe addition gérée avec unittest
    """
    def setUp(self):
        """ Executed before every test case """
        self.Calculator = SimpleCalculator()

    def test_somme(self):
        result = self.Calculator.somme(30, 5)
        self.assertEqual(result, 35)


class SoustractionTest(unittest.TestCase):
    """
    Classe soustraction gérée avec unittest
    """
    def setUp(self):
        """ Executed before every test case """
        self.Calculator = SimpleCalculator()

    def test_soustraction(self):
        result = self.Calculator.soustraction(30, 5)
        self.assertEqual(result, 25)


class MultiplicationTest(unittest.TestCase):
    """
    Classe multiplication gérée avec unittest
    """
    def setUp(self):
        """ Executed before every test case """
        self.Calculator = SimpleCalculator()

    def test_multiplication(self):
        result = self.Calculator.multiplication(30, 5)
        self.assertEqual(result, 150)


class DivisionTest(unittest.TestCase):
    """
    Classe division gérée avec unittest
    """
    def setUp(self):
        """ Executed before every test case """
        self.Calculator = SimpleCalculator()

    def test_division(self):
        result = self.Calculator.division(30, 5)
        self.assertEqual(result, 6)


if __name__ == "__main__":
    unittest.main()
